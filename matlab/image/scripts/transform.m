classdef transform < uint32

    enumeration
        dft_2d(0)
        DFT_2D(0)
        dft_3d(1)
        DFT_3D(1)
        fft(2)
        FFT(2)
    end
    
end

