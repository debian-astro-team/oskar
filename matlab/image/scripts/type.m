classdef type < uint32

    enumeration
        undef(0)
        UNDEF(0)
        stokes(1)
        STOKES(1)
        IQUV(1)
        I(2)
        Q(3)
        U(4)
        V(5)
        linear(6)
        LINEAR(6)
        XX(7)
        YY(8)
        XY(9)
        YX(10)
        
        PSF(50)
        
        beam_scalar(100)
        BEAM_SCALAR(100)
        beam_polarised(101)
        BEAM_POLARISED(101)
    end
    
end

